package com.cloud.course.beans.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cloud.common.entity.BaseEntity;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

/**
 * 课程信息
 *
 * @author Aijm
 * @date 2019-10-10 22:02:15
 */
@Data
@TableName("course")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "课程信息")
public class Course extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "类型（0：普通；1：精品；2：专题）")
    private Integer type;

    @ApiModelProperty(value = "分类ID集合(逗号分隔)")
    private String categoryIds;

    @ApiModelProperty(value = "分类ID")
    private Long categoryId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "课程封面")
    private String courseLogo;

    @ApiModelProperty(value = "是否免费：1免费，0收费")
    @TableField("is_free")
    private Integer hasFree;

    @ApiModelProperty(value = "原价")
    private BigDecimal courseOriginal;

    @ApiModelProperty(value = "优惠价")
    private BigDecimal courseDiscount;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "推荐（0：推荐，1：普通）")
    private Integer recommend;

    @ApiModelProperty(value = "级别")
    private Integer gradeLevel;

    @ApiModelProperty(value = "规划发布上架时间")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime putawayBegDate;

    @ApiModelProperty(value = "规划下架时间")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime putawayEndDate;

    @ApiModelProperty(value = "状态0:待发布 1：发布上架 2:下架")
    private Integer statusId;

    @ApiModelProperty(value = "审核状态(0:待审核,1:审核通过,2:审核不通过)")
    private Integer auditStatus;

    @ApiModelProperty(value = "审核意见")
    private String auditOpinion;

    @ApiModelProperty(value = "课程评分")
    private Integer score;

    @ApiModelProperty(value = "购买人数")
    private Integer countBuy;

    @ApiModelProperty(value = "学习人数")
    private Integer countStudy;

    @ApiModelProperty(value = "总课时数")
    private Integer periodTotal;

    @ApiModelProperty(value = "0:线下 1:线上")
    @TableField("`is_online`")
    private Integer hasOnline;

    @ApiModelProperty(value = "限制人数（当为线下时才有意义")
    private Integer limitNum;

    @ApiModelProperty(value = "上课地址")
    private String classAddr;

    // todo 时间可能存在bug修改
    @ApiModelProperty(value = "上课时间")
    private String begTime;

    @ApiModelProperty(value = "结课时间")
    private String endTime;



}
