package com.cloud.course.beans.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cloud.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.time.LocalDateTime;

/**
 * 课程用户关联表
 *
 * @author Aijm
 * @date 2019-10-13 16:40:58
 */
@Data
@TableName("course_user")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "课程用户关联表")
public class CourseUser extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "类型(0:加入学习，1:收藏课程)")
    private Integer type;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "总课时数")
    private Integer periodTotal;

    @ApiModelProperty(value = "已学习课时数")
    private Integer periodStudy;


    @ApiModelProperty(value = "加入课程是否免费：1免费，0收费")
    @TableField("is_free")
    private Integer hasFree;

    @ApiModelProperty(value = "加入课程是否购买：1购买，0未购买")
    @TableField("is_buy")
    private Integer hasBuy;


}
