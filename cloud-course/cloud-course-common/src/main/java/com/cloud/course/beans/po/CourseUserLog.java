package com.cloud.course.beans.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cloud.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 课程用户学习详细信息日志
 *
 * @author Aijm
 * @date 2019-10-13 16:46:28
 */
@Data
@TableName("course_user_log")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "课程用户学习详细信息日志")
public class CourseUserLog extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程编号")
    private Long courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "章节编号")
    private Long chapterId;

    @ApiModelProperty(value = "章节名称")
    private String chapterName;

    @ApiModelProperty(value = "课时编号")
    private Long periodId;

    @ApiModelProperty(value = "课时名称")
    private String periodName;

    @ApiModelProperty(value = "完成度0-100")
    private Integer complete;


}
