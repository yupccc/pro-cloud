package com.cloud.course.controller;

import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.course.beans.po.CourseUserLog;
import com.cloud.course.service.CourseUserLogService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.cloud.common.data.util.ObjUtil;

import javax.validation.Valid;

/**
 * 课程用户学习详细信息日志
 *
 * @author Aijm
 * @date 2019-10-13 16:46:28
 */
@RestController
@RequestMapping("/courseuserlog" )
@Api(value = "courseuserlog", tags = "课程用户学习详细信息日志管理")
public class CourseUserLogController {

    @Autowired
    private CourseUserLogService courseUserLogService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param courseUserLog 课程用户学习详细信息日志
     * @return
     */
    @GetMapping("/page")
    @PreAuthorize("@pms.hasPermission('course_courseuserlog_view')")
    public Result getCourseUserLogPage(Page page, CourseUserLog courseUserLog) {
        return Result.success(courseUserLogService.page(page, Wrappers.query(courseUserLog)));
    }


    /**
     * 通过id查询课程用户学习详细信息日志
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('course_courseuserlog_view')")
    public Result getById(@PathVariable("id") Long id) {
        return Result.success(courseUserLogService.getById(id));
    }

    /**
     * 新增课程用户学习详细信息日志
     * @param courseUserLog 课程用户学习详细信息日志
     * @return Result
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('course_courseuserlog_add')")
    public Result save(@RequestBody @Valid CourseUserLog courseUserLog) {
        return Result.success(courseUserLogService.save(courseUserLog));
    }

    /**
     * 修改课程用户学习详细信息日志
     * @param courseUserLog 课程用户学习详细信息日志
     * @return Result
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('course_courseuserlog_edit')")
    public Result updateById(@RequestBody @Valid CourseUserLog courseUserLog) {
        return Result.success(courseUserLogService.updateById(courseUserLog));
    }

    /**
     * 通过id删除课程用户学习详细信息日志
     * @param id id
     * @return Result
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('course_courseuserlog_del')")
    public Result removeById(@PathVariable Long id) {
        return Result.success(courseUserLogService.removeById(id));
    }

}
