package com.cloud.course.controller;

import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.course.beans.po.CourseUser;
import com.cloud.course.service.CourseUserService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.cloud.common.data.util.ObjUtil;

import javax.validation.Valid;

/**
 * 课程用户关联表
 *
 * @author Aijm
 * @date 2019-10-13 16:40:58
 */
@RestController
@RequestMapping("/courseuser" )
@Api(value = "courseuser", tags = "课程用户管理")
public class CourseUserController {

    @Autowired
    private CourseUserService courseUserService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param courseUser 课程用户关联表
     * @return
     */
    @GetMapping("/page")
    @PreAuthorize("@pms.hasPermission('course_courseuser_view')")
    public Result getCourseUserPage(Page page, CourseUser courseUser) {
        return Result.success(courseUserService.page(page, Wrappers.query(courseUser)));
    }


    /**
     * 通过id查询课程用户关联表
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('course_courseuser_view')")
    public Result getById(@PathVariable("id") Long id) {
        return Result.success(courseUserService.getById(id));
    }

    /**
     * 新增课程用户关联表
     * @param courseUser 课程用户关联表
     * @return Result
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('course_courseuser_add')")
    public Result save(@RequestBody @Valid CourseUser courseUser) {
        return Result.success(courseUserService.save(courseUser));
    }

    /**
     * 修改课程用户关联表
     * @param courseUser 课程用户关联表
     * @return Result
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('course_courseuser_edit')")
    public Result updateById(@RequestBody @Valid CourseUser courseUser) {
        return Result.success(courseUserService.updateById(courseUser));
    }

    /**
     * 通过id删除课程用户关联表
     * @param id id
     * @return Result
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('course_courseuser_del')")
    public Result removeById(@PathVariable Long id) {
        return Result.success(courseUserService.removeById(id));
    }

}
