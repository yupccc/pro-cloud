package com.cloud.course.callback;

import javax.servlet.http.HttpServletRequest;

import com.cloud.common.polyv.util.PolyvAuth;
import com.cloud.common.polyv.util.PolyvUtil;
import com.cloud.common.polyv.util.PolyvVideo;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import io.swagger.annotations.ApiOperation;

/**
 * 课时信息-审核
 * @author Aijm
 * @since 2019/10/8
 */
@RestController
@RequestMapping(value = "/callback/polyv")
public class CallbackPolyvController {


	/**
	 * 保利威视，视频上传回调接口
	 */
	@ApiOperation(value = "保利威视，视频上传回调接口", notes = "保利威视，视频上传回调接口")
	@GetMapping(value = "/video")
	public String callbackPolyvVideo(PolyvVideo polyvVideo) {
		return PolyvUtil.video(polyvVideo);
	}

	/**
	 * 保利威视，视频授权播放回调接口
	 */
	@ApiOperation(value = "保利威视，视频授权播放回调接口", notes = "保利威视，视频授权播放回调接口")
	@RequestMapping(value = "/auth", method = { RequestMethod.POST, RequestMethod.GET })
	public String callbackPolyvAuth(PolyvAuth polyvAuth, HttpServletRequest request) {
		if (StringUtils.isEmpty(polyvAuth.getCallback())) {
			String sourceParam = request.getQueryString();
			sourceParam = sourceParam.replaceAll("vid=" + polyvAuth.getVid(), "");
			sourceParam = sourceParam.replaceAll("&t=" + polyvAuth.getT(), "");
			sourceParam = sourceParam.replaceAll("&code=", "").replace("+", "%2B");
			polyvAuth.setCode(sourceParam);
		}
		return "";
	}

}
