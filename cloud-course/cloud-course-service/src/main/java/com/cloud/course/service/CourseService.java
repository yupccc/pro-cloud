package com.cloud.course.service;

import com.cloud.common.data.base.IService;
import com.cloud.course.beans.po.Course;

/**
 * 课程信息
 *
 * @author Aijm
 * @date 2019-10-10 22:02:15
 */
public interface CourseService extends IService<Course> {

}
