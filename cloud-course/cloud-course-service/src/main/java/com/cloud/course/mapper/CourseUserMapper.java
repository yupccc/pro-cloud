package com.cloud.course.mapper;

import com.cloud.common.data.base.BaseMapper;
import com.cloud.course.beans.po.CourseUser;

/**
 * 课程用户关联表
 *
 * @author Aijm
 * @date 2019-10-13 16:40:58
 */
public interface CourseUserMapper extends BaseMapper<CourseUser> {

}
