package com.cloud.common.data.util;


import com.cloud.common.data.user.SystemService;
import com.cloud.common.entity.BaseEntity;
import com.cloud.common.entity.TreeEntity;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 对象操作
 * @author Aijm
 * @since 2019/8/24
 */
public class ObjUtil {

    private static final SystemService systemService = SpringUtil.getBean(SystemService.class);

    /**
     * 插入之前执行方法，需要手动调用
     */
    public static <T extends BaseEntity> void preInsert(T entity){

        Long userId = systemService.getUserId();
        if (userId  != null) {
            entity.setCreateBy(userId);
            entity.setUpdateBy(userId);
        }
        entity.setId(IdUtil.getNextId());
        LocalDateTime now = LocalDateTime.now();
        entity.setCreateDate(now);
        entity.setUpdateDate(now);
        entity.setDelFlag(T.DEL_FLAG_NORMAL);
    }

    /**
     * 更新之前执行方法，需要手动调用
     */
    public static <T extends BaseEntity> void preUpdate(T entity){

        Long userId = systemService.getUserId();
        if (userId  != null) {
            entity.setUpdateBy(userId);
        }
        LocalDateTime now = LocalDateTime.now();
        entity.setUpdateDate(now);
    }

    /**
     * 树形 第一个节点的id
     */
    public static Long getRootId(){
        return 1L;
    }

    /**
     * 树 根据父子关系排序  该sourcelist 已经按照 sort 排序
     * @param list 排序后输出的存储对象
     * @param sourcelist 需要处理的数据
     * @param parentId  一般为1 父节点 第一个节点的id
     * @param cascade  一般为true
     */
    public static <T extends TreeEntity> void sortList(List<T> list, List<T> sourcelist, Long parentId, boolean cascade){
        int size = sourcelist.size();
        for (int i=0; i<size; i++){
            T e = sourcelist.get(i);
            if (parentId.equals(e.getParentId())){
                list.add(e);
                if (cascade){
                    // 判断是否还有子节点, 有则继续获取子节点
                    for (int j=0; j<size; j++){
                        T child = sourcelist.get(j);
                        if (child.getParentId().equals(e.getId())){
                            sortList(list, sourcelist, e.getId(), true);
                            break;
                        }
                    }
                }
            }
        }
    }

}
