package com.cloud.common.data.var;



/**
 * 全局配置
 * @author Aijm
 * @version 2019年8月31日
 */
public class Global {

	/**
	 * 是/否
	 */
	public static final String YES = "1";
	public static final String NO = "0";


}
