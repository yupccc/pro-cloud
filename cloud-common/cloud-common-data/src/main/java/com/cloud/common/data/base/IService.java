package com.cloud.common.data.base;

import com.cloud.common.entity.BaseEntity;

/**
 * @Author Aijm
 * @Description
 * @Date 2019/10/11
 */
public interface IService<T extends BaseEntity> extends com.baomidou.mybatisplus.extension.service.IService<T> {



}
