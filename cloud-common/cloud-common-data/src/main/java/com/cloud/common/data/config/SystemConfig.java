package com.cloud.common.data.config;

import com.cloud.common.data.user.SystemService;
import com.cloud.common.data.user.impl.SystemServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置bean
 * @author Aijm
 * @since 2019/8/25
 */
@Configuration
public class SystemConfig {

    /**
     * SystemService 实现
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(SystemService.class)
    public SystemService systemService() {
        return new SystemServiceImpl();
    }
}
