package com.cloud.common.cache.constants;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.cloud.common.cache.annotation.Cache;
import com.cloud.common.cache.annotation.CacheConf;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Method;

/**
 * cache 的key
 * @author Aijm
 * @since 2019/8/29
 */
public class CacheKeys {
    /**
     * 为null 时的字符串
     */
    public static final String EMPTY_OBJ = "${null}";

    /**
     * 为null 时的过期时间
     */
    public static final Long EXPIRETIME_NULL = 60L;


    /**
     * 用户缓存过期时间（包含用户查询到的菜单角色等）
     */
    public static final Long USER_EXPIRETIME = 36000L;

}
