package com.cloud.common.security.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


/**
 * @author Aijm
 * @date 2019/6/25
 * 接口权限判断工具
 */
@Slf4j
@Component("pms")
public class PermissionService {


	/**
	 * 判断接口是否有xxx:xxx权限
	 *
	 * @param permission 权限
	 * @return {boolean}
	 */
	public boolean hasPermission(String permission) {
		return true;
//		if (StrUtil.isBlank(permission)) {
//			return true;
//		}
//		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//		if (authentication == null) {
//			return false;
//		}
//		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
//		return authorities.stream()
//			.map(GrantedAuthority::getAuthority)
//			.filter(StringUtils::hasText)
//			.anyMatch(x -> PatternMatchUtils.simpleMatch(permission, x));
	}


	/**
	 * 判断当前用户是不是具有某个角色
	 * @param enname
	 * @return
	 */
	public boolean hasRole(String enname) {

		return true;
	}
}
