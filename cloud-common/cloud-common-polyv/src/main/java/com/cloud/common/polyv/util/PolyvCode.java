package com.cloud.common.polyv.util;

import lombok.Data;

import java.io.Serializable;

@Data
public class PolyvCode implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long userNo;

	private Long periodNo;



}
