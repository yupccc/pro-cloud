package com.cloud.common.polyv.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *
 * @author Aijm
 * @since 2019/10/6
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Question {

    /**
     * 视频id
     */
    private String vid;

    /**
     * 问题出现的秒数
     */
    private Integer seconds;

    /**
     * 问题的描述
     */
    private String question;

    /**
     * 答案，三个
     */
    private List<String> answerList;

    /**
     * 正确答案
     */
    private String rightAnswer;

    /**
     * 解答详情
     */
    private String answer;

    /**
     * 答错是否显示解答详情 1为显示，0不显示
     */
    private Integer wrongShow;

    /**
     * 答错退回秒数(第wrongTime秒),-1不回退
     */
    private Integer wrongTime;

    /**
     * 测试问题Id，为空的时候新创建一个，不为空则修改这条问题
     */
    private String examId;

    /**
     * 类型为整型，1表示正确答案，0表示错误答案
     */
    private int right;

    /**
     * //设置是否可以跳过问答，true表示可以跳过，不添加这个参数默认没有跳过的功能
     */
    private boolean skip;


}
