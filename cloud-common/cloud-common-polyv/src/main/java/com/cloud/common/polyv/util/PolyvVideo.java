package com.cloud.common.polyv.util;

import lombok.Data;

import java.io.Serializable;

@Data
public class PolyvVideo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String vid;

	private String sign;

	private String type;

	private String secretkey;


}
