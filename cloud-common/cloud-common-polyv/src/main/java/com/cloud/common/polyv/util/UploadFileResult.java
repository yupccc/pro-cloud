package com.cloud.common.polyv.util;

import lombok.Data;

import java.util.List;

/**
 * 上传文件返回结果
 * @author Aijm
 * @since 2019/10/6
 */
@Data
public class UploadFileResult {
	private List<String>  images_b;
	private String md5checksum;
	private String tag;
	private String mp4;
	private String title;
	private String df;
	private String times;
	private String mp4_1;
	private String vid;
	private String cataid;
	private String swf_link;
	private String source_filesize;
	private String status;
	private String seed;
	private String flv1;
	private String sourcefile;
	private String playerwidth;
	private String default_video;
	private String duration;
	private List<Long> filesize;
	private String first_image;
	private String original_definition;
	private String context;
	private List<String> images;
	private String playerheight;
	private String ptime;

}
