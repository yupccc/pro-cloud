package com.cloud.common.websocket.common;

/**
 * @Author Aijm
 * @Description WebSocket常量
 * @Date 2019/9/9
 */
public class WebSocketConsts {
    public static final String PUSH_SERVER = "/topic/server";
}
