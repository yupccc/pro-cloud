package com.cloud.common.util.base;

import com.cloud.common.util.enums.ResultEnum;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * @Author Aijm
 * @Description 返回公共实现
 * @Date 2019/5/8
 */
@Slf4j
@Data
public final class Result<T extends Serializable> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private Integer code = ResultEnum.ERROR.getCode();

    /**
     * 错误信息
     */
    private String msg = null;

    /**
     * 返回结果实体
     */
    private T data = null;

    public Result() {
    }

    private Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <T extends Serializable> Result<T> error(String msg) {
        log.debug("返回错误：code={}, msg={}", ResultEnum.ERROR.getCode(), msg);
        return new Result<T>(ResultEnum.ERROR.getCode(), msg, null);
    }

    public static <T extends Serializable> Result<T> error(ResultEnum resultEnum) {
        log.debug("返回错误：code={}, msg={}", resultEnum.getCode(), resultEnum.getDesc());
        return new Result<T>(resultEnum.getCode(), resultEnum.getDesc(), null);
    }

    public static <T extends Serializable> Result<T> error(int code, String msg) {
        log.debug("返回错误：code={}, msg={}", code, msg);
        return new Result<T>(code, msg, null);
    }

    public static <T extends Serializable> Result<T> success(T data) {
        return new Result<T>(ResultEnum.SUCCESS.getCode(), "", data);
    }


}
