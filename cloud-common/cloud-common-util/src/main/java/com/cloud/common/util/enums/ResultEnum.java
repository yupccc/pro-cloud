package com.cloud.common.util.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author Aijm
 * @Description 返回的code统一管理
 * @Date 2019/5/8
 */
@Getter
@AllArgsConstructor
public enum ResultEnum {
    // 成功
    SUCCESS(200, "成功"),
    // token异常  3开头
    TOKEN_PAST(301, "token过期"),
    TOKEN_ERROR(302, "token异常"),
    // 登录异常
    LOGIN_ERROR(303, "登录异常"),
    REMOTE_ERROR(304, "异地登录"),

    LOGIN_NAME(305, "用户名错误"),
    LOGIN_NAME_NULL(306, "用户名为空"),
    LOGIN_PASSWORD(307, "密码错误"),
    LOGIN_CODE(308, "验证码错误"),
    LOGOUT_CODE(309, "退出失败，token 为空"),


    // crud异常，4开头
    CRUD_SAVE_FAIL(403, "添加失败"),
    CRUD_UPDATE_FAIL(404, "更新失败"),
    CRUD_DELETE_FAIL(405, "删除失败"),
    CRUD_DELETE_NOT(406, "不允许删除"),

    CRUD_VALID_NOT(407, "字段校验异常"),

    COLLECTION(408, "已收藏"),
    USER_ADVICE(409, "保存建议失败,不能重复提建议"),

    // 用户异常，5开头
    LECTURER_REQUISITION_REGISTERED(501, "申请失败！该手机没注册，请先注册账号"),
    LECTURER_REQUISITION_WAIT(502, "申请失败！该账号已提交申请，待审核中，在7个工作日内会有相关人员与您联系确认"),
    LECTURER_REQUISITION_YET(503, "申请失败！该账号已提交申请，请直接登录"),
    //
    USER_SAVE_FAIL(504, "添加失败"),
    USER_UPDATE_FAIL(505, "更新失败"),

    // 错误
    ERROR(999, "错误");

    private Integer code;

    private String desc;
}
