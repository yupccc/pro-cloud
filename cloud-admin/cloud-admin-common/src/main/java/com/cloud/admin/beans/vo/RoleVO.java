package com.cloud.admin.beans.vo;

import com.cloud.admin.beans.dto.RoleDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 *  角色vo
 * @author Aijm
 * @since 2019/9/4
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleVO implements Serializable {

    private static final long serialVersionUID=1L;

    List<RoleDTO> roleList;
}
