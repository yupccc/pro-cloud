package com.cloud.admin.beans.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @Author Aijm
 * @Description 生成树
 * @Date 2019/9/5
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuTreeVO implements Serializable {

    private static final long serialVersionUID=1L;

    private List<Map<String, Object>> tree;
}
