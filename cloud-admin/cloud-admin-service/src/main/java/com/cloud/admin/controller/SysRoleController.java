package com.cloud.admin.controller;

import com.cloud.admin.beans.vo.RoleVO;
import com.cloud.admin.util.UserUtil;
import com.cloud.common.data.util.ObjUtil;
import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.admin.beans.po.SysRole;
import com.cloud.admin.service.SysRoleService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * 角色表
 *
 * @author Aijm
 * @date 2019-08-25 20:57:31
 */
@RestController
@RequestMapping("/sysrole" )
@Api(value = "sysrole", tags = "sysrole管理")
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 查询 全部 角色
     * @return
     */
    @GetMapping("/listALL")
    @PreAuthorize("@pms.hasPermission('admin_sysrole_view')")
    public Result getSysRoleAll() {
        return Result.success(RoleVO.builder().roleList(UserUtil.getRoleList()).build());
    }


    /**
     * 通过id查询角色表
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('admin_sysrole_view')")
    public Result getById(@PathVariable("id") Long id) {
        return Result.success(sysRoleService.getById(id));
    }

    /**
     * 新增角色表
     * @param sysRole 角色表
     * @return Result
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('admin_sysrole_add')")
    public Result save(@RequestBody @Valid SysRole sysRole) {
        return Result.success(sysRoleService.save(sysRole));
    }

    /**
     * 修改角色表
     * @param sysRole 角色表
     * @return Result
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('admin_sysrole_edit')")
    public Result updateById(@RequestBody @Valid SysRole sysRole) {
        return Result.success(sysRoleService.updateById(sysRole));
    }

    /**
     * 通过id删除角色表
     * @param id id
     * @return Result
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('admin_sysrole_del')")
    public Result removeById(@PathVariable Long id) {
        return Result.success(sysRoleService.removeById(id));
    }

}
