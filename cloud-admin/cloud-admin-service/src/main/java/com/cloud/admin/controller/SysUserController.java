package com.cloud.admin.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cloud.admin.beans.dto.UserDTO;
import com.cloud.admin.util.UserUtil;
import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.admin.beans.po.SysUser;
import com.cloud.admin.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * 用户表
 *
 * @author Aijm
 * @date 2019-08-25 20:20:58
 */
@RestController
@RequestMapping("/sysuser" )
@Api(value = "sysuser", tags = "sysuser管理")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 分页查询
     * @param page 分页对象 包含根据角色筛选
     * @param userDTO 用户表
     * @return
     */
    @GetMapping("/page")
    @PreAuthorize("@pms.hasPermission('admin_sysuser_view')")
    public Result getSysUserPage(Page page, UserDTO userDTO) {
        return Result.success(sysUserService.getSysUserPage(page, userDTO));
    }



    /**
     * 通过id查询用户表
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('admin_sysuser_view')")
    public Result getById(@PathVariable("id") Long id) {

        return Result.success(sysUserService.getById(id));
    }

    /**
     * 新增用户表
     * @param userDTO 用户表
     * @return Result
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('admin_sysuser_add')")
    public Result save(@RequestBody @Valid UserDTO userDTO) {
        // 角色数据有效性验证，过滤不在授权内的角色
        // todo
//        UserDTO user = UserUtil.getUser();
//        List<Long> roleIdList = user.getRoleIdList();

        return Result.success(sysUserService.saveUserDTO(userDTO));
    }

    /**
     * 修改用户表
     * @param userDTO 用户表
     * @return Result
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('admin_sysuser_edit')")
    public Result updateById(@RequestBody @Valid UserDTO userDTO) {
        // 角色数据有效性验证，过滤不在授权内的角色
        // todo
        return Result.success(sysUserService.updateUserDTO(userDTO));
    }

    /**
     * 通过id删除用户表
     * @param id id
     * @return Result
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('admin_sysuser_del')")
    public Result removeById(@PathVariable Long id) {
        return Result.success(sysUserService.removeUserDTO(id));
    }

}
