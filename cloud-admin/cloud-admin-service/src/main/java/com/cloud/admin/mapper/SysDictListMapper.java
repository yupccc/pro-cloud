package com.cloud.admin.mapper;

import com.cloud.admin.beans.po.SysDictList;
import com.cloud.common.data.base.BaseMapper;

/**
 * 字典项list
 *
 * @author Aijm
 * @date 2019-09-05 19:52:37
 */
public interface SysDictListMapper extends BaseMapper<SysDictList> {

}
