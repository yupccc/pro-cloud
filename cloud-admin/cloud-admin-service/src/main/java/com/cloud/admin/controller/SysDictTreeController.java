package com.cloud.admin.controller;

import com.cloud.admin.beans.vo.DictTreeVO;
import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.admin.beans.po.SysDictTree;
import com.cloud.admin.service.SysDictTreeService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.cloud.common.data.util.ObjUtil;

import javax.validation.Valid;

/**
 * 字典表树
 *
 * @author Aijm
 * @date 2019-09-05 20:00:25
 */
@RestController
@RequestMapping("/sysdicttree" )
@Api(value = "sysdicttree", tags = "sysdicttree管理")
public class SysDictTreeController {

    @Autowired
    private SysDictTreeService sysDictTreeService;


    /**
     * 通过id查询字典表树
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('admin_sysdicttree_view')")
    public Result getById(@PathVariable("id") Long id) {
        return Result.success(sysDictTreeService.getById(id));
    }

    /**
     * 通过typecode查询字典项list 树结构
     * @param typeCode typeCode
     * @return Result
     */
    @GetMapping("/type/{typeCode}")
    @PreAuthorize("@pms.hasPermission('admin_sysdicttree_view')")
    public Result getByTypeCode(@PathVariable("typeCode") String typeCode) {
        return Result.success(DictTreeVO.builder()
                .dictTreeList(sysDictTreeService.getDicTreeByType(typeCode)).build());
    }
    /**
     * 新增字典表树
     * @param sysDictTree 字典表树
     * @return Result
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('admin_sysdicttree_add')")
    public Result save(@RequestBody @Valid SysDictTree sysDictTree) {
        return Result.success(sysDictTreeService.save(sysDictTree));
    }

    /**
     * 修改字典表树
     * @param sysDictTree 字典表树
     * @return Result
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('admin_sysdicttree_edit')")
    public Result updateById(@RequestBody @Valid SysDictTree sysDictTree) {
        return Result.success(sysDictTreeService.updateById(sysDictTree));
    }

    /**
     * 通过id删除字典表树
     * @param id id
     * @return Result
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('admin_sysdicttree_del')")
    public Result removeById(@PathVariable Long id) {
        SysDictTree byId = sysDictTreeService.getById(id);
        return Result.success(sysDictTreeService.removeByDictTree(byId));
    }

}
