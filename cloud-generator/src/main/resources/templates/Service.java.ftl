package ${package}.${moduleName}.service;

import com.cloud.common.data.base.IService;
import ${package}.${moduleName}.beans.po.${className};

/**
 * ${comments}
 *
 * @author ${author}
 * @date ${datetime}
 */
public interface ${className}Service extends IService<${className}> {

}
