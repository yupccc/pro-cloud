package com.cloud.email.api;


import com.cloud.common.util.base.Result;
import com.cloud.common.util.client.CloudServiceList;
import com.cloud.email.beans.dto.SendEmailDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * @Author Aijm
 * @Description  feign 邮件暴露接口
 * @Date 2019/9/11
 */
@FeignClient(value = CloudServiceList.CLOUD_SMS)
public interface ISendEmailApi {


    /**
     * 发送邮件
     * @param sendEmail
     * @return
     */
    @RequestMapping(value="/email/sendEmail", method= RequestMethod.POST)
    Result sendEmail(@RequestBody @Valid SendEmailDTO sendEmail);
}
