package com.cloud.exam.service;

import com.cloud.common.data.base.IService;
import com.cloud.exam.beans.po.ExamUserAnswer;

/**
 * 测试结果详细信息
 *
 * @author Aijm
 * @date 2019-10-14 23:32:31
 */
public interface ExamUserAnswerService extends IService<ExamUserAnswer> {

}
