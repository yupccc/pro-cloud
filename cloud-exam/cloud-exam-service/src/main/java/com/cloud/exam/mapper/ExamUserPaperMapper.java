package com.cloud.exam.mapper;

import com.cloud.common.data.base.BaseMapper;
import com.cloud.exam.beans.po.ExamUserPaper;

/**
 * 问题详细信息
 *
 * @author Aijm
 * @date 2019-10-14 23:14:21
 */
public interface ExamUserPaperMapper extends BaseMapper<ExamUserPaper> {

}
