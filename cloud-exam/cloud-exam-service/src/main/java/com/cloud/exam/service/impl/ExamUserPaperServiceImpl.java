package com.cloud.exam.service.impl;

import com.cloud.common.data.base.BaseService;
import com.cloud.exam.beans.po.ExamUserPaper;
import com.cloud.exam.mapper.ExamUserPaperMapper;
import com.cloud.exam.service.ExamUserPaperService;
import org.springframework.stereotype.Service;

/**
 * 问题详细信息
 *
 * @author Aijm
 * @date 2019-10-14 23:14:21
 */
@Service
public class ExamUserPaperServiceImpl extends BaseService<ExamUserPaperMapper, ExamUserPaper> implements ExamUserPaperService {

}
