package com.cloud.exam.controller;

import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.exam.beans.po.ExamQuestion;
import com.cloud.exam.service.ExamQuestionService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.cloud.common.data.util.ObjUtil;

import javax.validation.Valid;

/**
 * 题目详细信息
 *
 * @author Aijm
 * @date 2019-10-14 23:00:15
 */
@RestController
@RequestMapping("/examquestion" )
@Api(value = "examquestion", tags = "题目管理")
public class ExamQuestionController {

    @Autowired
    private ExamQuestionService examQuestionService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param examQuestion 问题详细信息
     * @return
     */
    @GetMapping("/page")
    @PreAuthorize("@pms.hasPermission('exam_examquestion_view')")
    public Result getExamQuestionPage(Page page, ExamQuestion examQuestion) {
        return Result.success(examQuestionService.page(page, Wrappers.query(examQuestion)));
    }


    /**
     * 通过id查询问题详细信息
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('exam_examquestion_view')")
    public Result getById(@PathVariable("id") Long id) {
        return Result.success(examQuestionService.getById(id));
    }

    /**
     * 新增问题详细信息
     * @param examQuestion 问题详细信息
     * @return Result
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('exam_examquestion_add')")
    public Result save(@RequestBody @Valid ExamQuestion examQuestion) {
        return Result.success(examQuestionService.save(examQuestion));
    }

    /**
     * 修改问题详细信息
     * @param examQuestion 问题详细信息
     * @return Result
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('exam_examquestion_edit')")
    public Result updateById(@RequestBody @Valid ExamQuestion examQuestion) {
        return Result.success(examQuestionService.updateById(examQuestion));
    }

    /**
     * 通过id删除问题详细信息
     * @param id id
     * @return Result
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('exam_examquestion_del')")
    public Result removeById(@PathVariable Long id) {
        return Result.success(examQuestionService.removeById(id));
    }

}
