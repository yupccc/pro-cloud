package com.cloud.exam.controller;

import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.exam.beans.po.ExamPaper;
import com.cloud.exam.service.ExamPaperService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.cloud.common.data.util.ObjUtil;

import javax.validation.Valid;

/**
 * 试卷详细信息
 *
 * @author Aijm
 * @date 2019-10-13 23:30:51
 */
@RestController
@RequestMapping("/exampaper" )
@Api(value = "exampaper", tags = "试卷详细信息管理")
public class ExamPaperController {

    @Autowired
    private ExamPaperService examPaperService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param examPaper 试卷详细信息
     * @return
     */
    @GetMapping("/page")
    @PreAuthorize("@pms.hasPermission('exam_exampaper_view')")
    public Result getExamPaperPage(Page page, ExamPaper examPaper) {
        return Result.success(examPaperService.page(page, Wrappers.query(examPaper)));
    }


    /**
     * 通过id查询试卷详细信息
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('exam_exampaper_view')")
    public Result getById(@PathVariable("id") Long id) {
        return Result.success(examPaperService.getById(id));
    }

    /**
     * 新增试卷详细信息
     * @param examPaper 试卷详细信息
     * @return Result
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('exam_exampaper_add')")
    public Result save(@RequestBody @Valid ExamPaper examPaper) {
        return Result.success(examPaperService.save(examPaper));
    }

    /**
     * 修改试卷详细信息
     * @param examPaper 试卷详细信息
     * @return Result
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('exam_exampaper_edit')")
    public Result updateById(@RequestBody @Valid ExamPaper examPaper) {
        return Result.success(examPaperService.updateById(examPaper));
    }

    /**
     * 通过id删除试卷详细信息
     * @param id id
     * @return Result
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('exam_exampaper_del')")
    public Result removeById(@PathVariable Long id) {
        return Result.success(examPaperService.removeById(id));
    }

}
