package com.cloud.exam.controller;

import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.exam.beans.po.ExamUserAnswer;
import com.cloud.exam.service.ExamUserAnswerService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.cloud.common.data.util.ObjUtil;

import javax.validation.Valid;

/**
 * 测试结果详细信息
 *
 * @author Aijm
 * @date 2019-10-14 23:32:31
 */
@RestController
@RequestMapping("/examuseranswer" )
@Api(value = "examuseranswer", tags = "用户作答管理")
public class ExamUserAnswerController {

    @Autowired
    private ExamUserAnswerService examUserAnswerService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param examUserAnswer 测试结果详细信息
     * @return
     */
    @GetMapping("/page")
    @PreAuthorize("@pms.hasPermission('exam_examuseranswer_view')")
    public Result getExamUserAnswerPage(Page page, ExamUserAnswer examUserAnswer) {
        return Result.success(examUserAnswerService.page(page, Wrappers.query(examUserAnswer)));
    }


    /**
     * 通过id查询测试结果详细信息
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('exam_examuseranswer_view')")
    public Result getById(@PathVariable("id") Long id) {
        return Result.success(examUserAnswerService.getById(id));
    }

    /**
     * 新增测试结果详细信息
     * @param examUserAnswer 测试结果详细信息
     * @return Result
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('exam_examuseranswer_add')")
    public Result save(@RequestBody @Valid ExamUserAnswer examUserAnswer) {
        return Result.success(examUserAnswerService.save(examUserAnswer));
    }

    /**
     * 修改测试结果详细信息
     * @param examUserAnswer 测试结果详细信息
     * @return Result
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('exam_examuseranswer_edit')")
    public Result updateById(@RequestBody @Valid ExamUserAnswer examUserAnswer) {
        return Result.success(examUserAnswerService.updateById(examUserAnswer));
    }

    /**
     * 通过id删除测试结果详细信息
     * @param id id
     * @return Result
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('exam_examuseranswer_del')")
    public Result removeById(@PathVariable Long id) {
        return Result.success(examUserAnswerService.removeById(id));
    }

}
