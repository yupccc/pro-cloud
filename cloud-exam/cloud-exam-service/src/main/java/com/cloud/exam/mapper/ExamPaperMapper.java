package com.cloud.exam.mapper;

import com.cloud.common.data.base.BaseMapper;
import com.cloud.exam.beans.po.ExamPaper;

/**
 * 试卷详细信息
 *
 * @author Aijm
 * @date 2019-10-13 23:30:51
 */
public interface ExamPaperMapper extends BaseMapper<ExamPaper> {

}
