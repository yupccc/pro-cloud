package com.cloud.exam.service.impl;

import com.cloud.common.data.base.BaseService;
import com.cloud.exam.beans.po.ExamQuestion;
import com.cloud.exam.mapper.ExamQuestionMapper;
import com.cloud.exam.service.ExamQuestionService;
import org.springframework.stereotype.Service;

/**
 * 问题详细信息
 *
 * @author Aijm
 * @date 2019-10-14 23:00:15
 */
@Service
public class ExamQuestionServiceImpl extends BaseService<ExamQuestionMapper, ExamQuestion> implements ExamQuestionService {

}
