package com.cloud.exam.beans.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cloud.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 问题详细信息
 *
 * @author Aijm
 * @date 2019-10-14 23:00:15
 */
@Data
@TableName("exam_question")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "问题详细信息")
public class ExamQuestion extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分类ID集合(逗号分隔)")
    private Long categoryIds;

    @ApiModelProperty(value = "分类ID")
    private Long categoryId;

    @ApiModelProperty(value = "1.单选题  2.多选题  3.判断题 4.填空题 5.简答题")
    private Integer questionType;

    @ApiModelProperty(value = "题目总分(千分制)")
    private Integer score;

    @ApiModelProperty(value = "级别")
    private Integer gradeLevel;

    @ApiModelProperty(value = "题目难度")
    private Integer difficult;

    @ApiModelProperty(value = "正确答案")
    private String correct;

    @ApiModelProperty(value = "试卷框架 内容为JSON 题目  填空、 题干、解析、答案等信息")
    private String textContent;


    // todo 可以考虑对应用户的评论
}
