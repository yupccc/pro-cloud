package com.cloud.sms;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.cloud.common.data.var.Global;
import com.cloud.common.util.exception.BaseException;
import com.cloud.sms.beans.dto.SendMsgDTO;
import com.cloud.sms.beans.dto.SmsConfigDTO;
import com.cloud.sms.beans.po.SysMsg;
import com.cloud.sms.beans.po.SysMsgConfig;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.Map;


@Slf4j
public class TestApplication {


    @Test
    public void aa(){

        SysMsgConfig config = new SysMsgConfig();
        config.setSenderName("eduvipxcode");
        config.setSmsPlatAccount("LTAI4Fr8jHtG2Y9NSTRT6Nqd");
        config.setSmsPlatPassword("nxoRx5lvvfYUWcfFgPaPwtHMQGsQVz");
        config.setSmsPlatUrl("dysmsapi.aliyuncs.com");

        Map<String, String> map = Maps.newHashMap();
        map.put("code", "22288");
        map.put("codev", "26622");
        SendMsgDTO sendMsg = SendMsgDTO.builder().bizType("0").bizType("2")
                .mobile("18210584253").map(map).params(JSONUtil.toJsonStr(map)).build();
        String templateCode = "SMS_173950628";

        String sender = StringUtils.defaultIfBlank("", config.getSenderName());

        DefaultProfile profile = DefaultProfile.getProfile("default" ,config.getSmsPlatAccount(),
                config.getSmsPlatPassword());

        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain(config.getSmsPlatUrl());
        request.setAction(SmsConfigDTO.SEND_SMS);
        request.setVersion(SmsConfigDTO.SMS_VSERSION);

        request.putQueryParameter("PhoneNumbers", sendMsg.getMobile());
        request.putQueryParameter("RegionId", SmsConfigDTO.SMS_REGION_ID);

        request.putQueryParameter("SignName", sender);
        request.putQueryParameter("TemplateCode", templateCode);
        if (StrUtil.isNotBlank(sendMsg.getParams())) {
            request.putQueryParameter("TemplateParam", sendMsg.getParams());
        }
        try {
            CommonResponse response = client.getCommonResponse(request);

            SysMsg record = new SysMsg();
            JSONObject json = JSONUtil.parseObj(response.getData());

            record.setRequestId(json.getStr("RequestId"));
            record.setType(sendMsg.getBizType());
            record.setMobile(sendMsg.getMobile());
            record.setContent(sendMsg.getParams());
            if (SmsConfigDTO.SMS_CODE_OK.equals(json.getStr("Code"))) {
                record.setSendState(Integer.valueOf(Global.YES));
            } else {
                record.setSendState(Integer.valueOf(Global.NO));
            }


        log.info("jies");
        } catch (Exception e) {
            log.info("ji4444es");
            throw new RuntimeException(e.getMessage());
        }
    }
}
