package com.cloud.sms.service.impl;

import com.cloud.common.data.base.BaseService;
import com.cloud.sms.beans.po.SysMsg;
import com.cloud.sms.mapper.SysMsgMapper;
import com.cloud.sms.service.SysMsgService;
import org.springframework.stereotype.Service;

/**
 * 短信发送消息日志
 *
 * @author Aijm
 * @date 2019-09-10 13:45:16
 */
@Service
public class SysMsgServiceImpl extends BaseService<SysMsgMapper, SysMsg> implements SysMsgService {

}
