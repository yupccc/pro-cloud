package com.cloud.sms;

import com.cloud.common.cache.annotation.EnableProCache;
import com.cloud.common.data.annotation.EnableProData;
import com.cloud.common.security.annotation.EnableProSecurtity;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 *   基础模块
 * @author Aijm
 * @since  2019/5/8
 */
@SpringCloudApplication
@EnableProData
@EnableProSecurtity
@EnableProCache
public class CloudSmsApplication {


    public static void main(String[] args) {
        SpringApplication.run(CloudSmsApplication.class, args);
    }


}
