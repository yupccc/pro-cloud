package com.cloud.sms.service.impl;

import com.cloud.common.data.base.BaseService;
import com.cloud.sms.beans.po.SysMsgConfig;
import com.cloud.sms.mapper.SysMsgConfigMapper;
import com.cloud.sms.service.SysMsgConfigService;
import org.springframework.stereotype.Service;

/**
 * 短信配置信息
 *
 * @author Aijm
 * @date 2019-09-10 13:40:14
 */
@Service
public class SysMsgConfigServiceImpl extends BaseService<SysMsgConfigMapper, SysMsgConfig> implements SysMsgConfigService {

}
